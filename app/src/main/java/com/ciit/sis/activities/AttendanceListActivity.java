package com.ciit.sis.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.adapters.AttendanceListAdapter;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Attendance;
import com.ciit.sis.beans.User;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.GetAttendanceResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttendanceListActivity extends BaseActivity {

    private RecyclerView rvAttendance;
    private AttendanceListAdapter listAdapter;
    private List<Attendance> attendanceList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Attendance");
        context = this;
        rvAttendance = findViewById(R.id.rv_attendance);
        Button btnAddAttendance = findViewById(R.id.btn_add_attendance);
        if(AppData.getInstance().getUser().getUserable_type().endsWith("Teacher")){
            btnAddAttendance.setVisibility(View.VISIBLE);
        }
        showProgressDialog("Loading Attendance...");
        User user = AppData.getInstance().getUser();
        int studentId = user.getUserable_id();
        if(user.getUserable_type().equals("Guardian")){
            studentId = Integer.parseInt(user.getStudent_id());
        }
        ApiInterface apiService = ApiUtils.getAPIService();
        Call<GetAttendanceResponse> call = apiService.getAttendance(studentId);
        call.enqueue(new Callback<GetAttendanceResponse>() {
            @Override
            public void onResponse(Call<GetAttendanceResponse> call, Response<GetAttendanceResponse> response) {
                if(response.body() ==null || !response.body().isSuccess()){
                    Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
                    return;
                }
                attendanceList = response.body().getData();
                updateList();
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<GetAttendanceResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void updateList(){
        listAdapter = new AttendanceListAdapter(attendanceList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rvAttendance.setLayoutManager(linearLayoutManager);
        rvAttendance.setAdapter(listAdapter);
    }

    public void addAttendance(View view) {
    }
}
