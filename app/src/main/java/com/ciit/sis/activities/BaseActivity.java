package com.ciit.sis.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

  Context context;
  ProgressDialog progressDialog;

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (progressDialog != null && progressDialog.isShowing()) {
      progressDialog.cancel();
    }
    progressDialog = null;

  }

  void showProgressDialog(String msg) {
    if (progressDialog == null) {
      progressDialog = new ProgressDialog(context);
      progressDialog.setCanceledOnTouchOutside(false);
      progressDialog.setIndeterminate(true);
    }
    progressDialog.setMessage(msg);
    progressDialog.show();
    progressDialog.setCancelable(false);
    progressDialog.setCanceledOnTouchOutside(false);
  }

  void hideProgressDialog() {
    Handler mainHandler = new Handler(context.getMainLooper());
    mainHandler.post(new Runnable() {
      @Override
      public void run() {
        if (progressDialog != null && progressDialog.isShowing()) {
          progressDialog.hide();
          progressDialog.dismiss();
          progressDialog = null;
        }
      }
    });
  }
}
