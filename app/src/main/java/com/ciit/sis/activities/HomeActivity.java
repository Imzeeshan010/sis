package com.ciit.sis.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.ciit.sis.R;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.User;

public class HomeActivity extends BaseActivity{
    private Button btnSubjects, btnResult, btnResultChild, btnUploadResult, btnAttendance, btnAttendanceChild, btnMarkAttendance;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_home);
        btnSubjects = findViewById(R.id.btn_subjects);
        btnResult = findViewById(R.id.btn_results);
        btnResultChild = findViewById(R.id.btn_results_child);
        btnUploadResult = findViewById(R.id.btn_upload_results);
        btnAttendance = findViewById(R.id.btn_attendance);
        btnAttendanceChild = findViewById(R.id.btn_attendance_child);
        btnMarkAttendance = findViewById(R.id.btn_mark_attendance);
        User user = AppData.getInstance().getUser();
        if(user!=null && user.getUserable_type().equals("Student")){
            btnUploadResult.setVisibility(View.GONE);
            btnMarkAttendance.setVisibility(View.GONE);
            btnAttendanceChild.setVisibility(View.GONE);
            btnResultChild.setVisibility(View.GONE);
        }
        else if(user!=null && user.getUserable_type().endsWith("Teacher")){
            btnResult.setVisibility(View.GONE);
            btnAttendance.setVisibility(View.GONE);
            btnAttendanceChild.setVisibility(View.GONE);
            btnResultChild.setVisibility(View.GONE);
        }
        else if(user!=null && user.getUserable_type().endsWith("Guardian")){
            btnUploadResult.setVisibility(View.GONE);
            btnSubjects.setVisibility(View.GONE);
            btnMarkAttendance.setVisibility(View.GONE);
            btnResult.setVisibility(View.GONE);
            btnAttendance.setVisibility(View.GONE);
        }
    }

    public void onViewSubjectsPressed(View view) {
        startActivity(new Intent(context, SubjectsListActivity.class));
    }


    public void onViewResultsPressed(View view) {
        startActivity(new Intent(context, ResultListActivity.class));
    }

    public void onViewAttendancePressed(View view) {
        startActivity(new Intent(context, AttendanceListActivity.class));
    }

    public void onLogoutPressed(View view) {
        AppData.getInstance().setUser(null);
        AppData.getInstance().setSubjects(null);
        startActivity(new Intent(context, LoginActivity.class));
        finish();
    }

    public void onMarkAttendancePressed(View view) {
        startActivity(new Intent(context, MarkAttendanceActivity.class));
    }

    public void onUploadResultsPressed(View view) {
        startActivity(new Intent(context, UploadMarksActivity.class));
    }

    public void onViewChildAttendancePressed(View view) {

    }

    public void onViewChildResultsPressed(View view) {

    }
}
