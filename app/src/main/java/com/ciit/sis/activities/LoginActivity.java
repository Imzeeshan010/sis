package com.ciit.sis.activities;

import android.content.Intent;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.app.AppData;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private EditText etEmail;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        etEmail = findViewById(R.id.et_email);

        etPassword = findViewById(R.id.et_password);
        Button btnSubmit = findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    private void attemptLogin() {

        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        if (!TextUtils.isEmpty(password) && password.length() < 4) {
            Toast.makeText(this, "This password is too short", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(email) && email.contains("@")) {
            Toast.makeText(this, "Enter valid email", Toast.LENGTH_SHORT).show();
            return;
        }

        showProgressDialog("Logging In...");

        ApiInterface apiService = ApiUtils.getAPIService();
        Call<LoginResponse> call = apiService.login(email, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if(response.body() == null){
                    Toast.makeText(context, "There is some problem try again", Toast.LENGTH_SHORT).show();
                }
                else if(response.body().isSuccess()){
                    AppData.getInstance().setUser(response.body().getData().getUser());
                    AppData.getInstance().setSubjects(response.body().getData().getSubjects());
                    startActivity(new Intent(context, HomeActivity.class));
                    finish();
                }
                else{
                    Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(context, "There is some problem try again", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

