package com.ciit.sis.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.adapters.ResultsListAdapter;
import com.ciit.sis.adapters.StudentsListAdapter;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Student;
import com.ciit.sis.beans.Subject;
import com.ciit.sis.beans.User;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.GetResultResponse;
import com.ciit.sis.net.models.GetStudentsResponse;
import com.ciit.sis.net.models.PostResponse;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarkAttendanceActivity extends BaseActivity {
    private EditText etDate;
    private Button btnSubmit;
    private Spinner spnSubjects;
    private List<Student> studentList;
    private RecyclerView rvStudents;
    private StudentsListAdapter listAdapter;
    final Calendar myCalendar = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_attendance);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Mark Attendance");
        context = this;
        etDate = findViewById(R.id.et_date);
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(context, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMinDate(System.currentTimeMillis()-1000);
                dialog.show();
            }
        });
        btnSubmit = findViewById(R.id.btn_submit);
        spnSubjects = findViewById(R.id.spn_subjects);
        rvStudents = findViewById(R.id.rv_students);
        ArrayAdapter<Subject> subjectArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, AppData.getInstance().getSubjects());
        spnSubjects.setAdapter(subjectArrayAdapter);
        spnSubjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Subject subject = AppData.getInstance().getSubjects().get(i);
                loadStudents(subject.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if(AppData.getInstance().getSubjects().size()==0){
            Toast.makeText(context, "No subject for attendance.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
    private void loadStudents(int subjectId){
        showProgressDialog("Loading Attendance...");

        ApiInterface apiService = ApiUtils.getAPIService();
        Call<GetStudentsResponse> call = apiService.getStudents(subjectId);
        call.enqueue(new Callback<GetStudentsResponse>() {
            @Override
            public void onResponse(Call<GetStudentsResponse> call, Response<GetStudentsResponse> response) {
                hideProgressDialog();
                if(response.body() ==null || !response.body().isSuccess()){
                    Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
                    return;
                }
                studentList = response.body().getData();
                updateList();
            }

            @Override
            public void onFailure(Call<GetStudentsResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateList(){
        if(studentList.size()>0){
            btnSubmit.setVisibility(View.VISIBLE);
        }
        listAdapter = new StudentsListAdapter(studentList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rvStudents.setLayoutManager(linearLayoutManager);
        rvStudents.setAdapter(listAdapter);
    }

    public void submitAttendance(View view) {
        if(etDate.getText()==null || etDate.getText().toString().equals("")){
            Toast.makeText(context, "Add date.", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog("Saving attendance");
        ArrayList<Integer> presentStudents = new ArrayList<>();
        for(Student student:studentList){
             Log.d("Student Attendance"+ student.getId()," "+student.isPresent());
            if(student.isPresent()){
                presentStudents.add(student.getId());
            }
        }
        Subject subjectSelected = (Subject) spnSubjects.getSelectedItem();
        Map<String, String> params = new HashMap<>();
        params.put("date",""+etDate.getText().toString());
        params.put("subjectId",""+subjectSelected.getId());
        params.put("studentIds",""+presentStudents);
        ApiInterface apiService = ApiUtils.getAPIService();
        Call<PostResponse> call = apiService.submitAttendance(params);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                hideProgressDialog();
                if(response.body() ==null || !response.body().isSuccess()){
                    Toast.makeText(context, "Can't save Attendance try again later", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(context,"Attendance submitted", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "Can't save attendace try again later", Toast.LENGTH_SHORT).show();
            }
        });

    }
    private void updateDate(){
        String myFormat = "yyyy-MM-DD"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etDate.setText(sdf.format(myCalendar.getTime()));
    }
    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDate();
        }

    };
}
