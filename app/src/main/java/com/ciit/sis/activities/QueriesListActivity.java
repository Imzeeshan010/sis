package com.ciit.sis.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.adapters.QueriesListAdapter;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Query;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.GetQueriesResponse;
import com.ciit.sis.net.models.PostResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QueriesListActivity extends BaseActivity {
    private int subjectId;
    private RecyclerView rvQueries;
    private QueriesListAdapter listAdapter;
    private List<Query> queryList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queries_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Queries");
        context = this;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            subjectId = bundle.getInt("subjectId", -1);
        }
        rvQueries = findViewById(R.id.rv_queries);

        showProgressDialog("Loading Queries...");

        ApiInterface apiService = ApiUtils.getAPIService();
        Call<GetQueriesResponse> call = apiService.getQueries(subjectId);
        call.enqueue(new Callback<GetQueriesResponse>() {
            @Override
            public void onResponse(Call<GetQueriesResponse> call, Response<GetQueriesResponse> response) {
                if(!response.body().isSuccess()){
                    Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
                    return;
                }
                queryList = response.body().getData();
                updateList();
                hideProgressDialog();
            }

            @Override
            public void onFailure(Call<GetQueriesResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateList(){
        listAdapter = new QueriesListAdapter(queryList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rvQueries.setLayoutManager(linearLayoutManager);
        rvQueries.setAdapter(listAdapter);
    }
    public void addQuery(View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_add_query, null);
        final EditText etQuery = dialogView.findViewById(R.id.et_query);

        final AlertDialog alertDialog = builder.setView(dialogView)
                .setTitle("Add New Query")
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Add", null).create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {
                Button btnPositive = ((AlertDialog) dialogInterface)
                        .getButton(DialogInterface.BUTTON_POSITIVE);
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String query = etQuery.getText().toString();
                        if (TextUtils.isEmpty(query)) {
                            Toast.makeText(context, "Enter query", Toast.LENGTH_SHORT).show();
                        } else {
                            postQueryToServer(query);
                        }
                    }
                });

            }
        });
        alertDialog.show();
    }
    private void postQueryToServer(final String queryText){
        showProgressDialog("Saving Queries...");
        final int studentId = AppData.getInstance().getUser().getUserable_id();

        Map<String, String> params = new HashMap<>();
        params.put("studentId",""+studentId);
        params.put("subjectId",""+subjectId);
        params.put("query",queryText);
        params.put("answer","");

        ApiInterface apiService = ApiUtils.getAPIService();
        Call<PostResponse> call = apiService.postQuery(params);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                hideProgressDialog();
                if(response.body()==null || !response.body().isSuccess()){
                    Toast.makeText(context, "Problem posting query. Try again later.", Toast.LENGTH_SHORT).show();
                    return;
                }
                Query query = new Query();
                query.setStudent_id(studentId);
                query.setSubject_id(subjectId);
                query.setQuestion(queryText);
                queryList.add(query);
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "Problem posting query. Try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
