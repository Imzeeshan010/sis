package com.ciit.sis.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.adapters.AttendanceListAdapter;
import com.ciit.sis.adapters.ResultsListAdapter;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Attendance;
import com.ciit.sis.beans.Result;
import com.ciit.sis.beans.User;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.GetAttendanceResponse;
import com.ciit.sis.net.models.GetResultResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultListActivity extends BaseActivity {
    private RecyclerView rvResults;
    private ResultsListAdapter listAdapter;
    private List<Result> resultsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Results");
        context = this;
        rvResults = findViewById(R.id.rv_results);
        Button btnAddAttendance = findViewById(R.id.btn_add_attendance);
        if(AppData.getInstance().getUser().getUserable_type().endsWith("Teacher")){
            btnAddAttendance.setVisibility(View.VISIBLE);
        }
        showProgressDialog("Loading Attendance...");
        User user = AppData.getInstance().getUser();
        int studentId = user.getUserable_id();
        if(user.getUserable_type().equals("Guardian")){
            studentId = Integer.parseInt(user.getStudent_id());
        }
        ApiInterface apiService = ApiUtils.getAPIService();
        Call<GetResultResponse> call = apiService.getResult(studentId);
        call.enqueue(new Callback<GetResultResponse>() {
            @Override
            public void onResponse(Call<GetResultResponse> call, Response<GetResultResponse> response) {
                hideProgressDialog();
                if(response.body() ==null || response.body().isSuccess()==false ){
                    Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
                    return;
                }
                resultsList = new ArrayList<>(response.body().getData().values());
                updateList();
            }

            @Override
            public void onFailure(Call<GetResultResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    private void updateList(){
        listAdapter = new ResultsListAdapter(resultsList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rvResults.setLayoutManager(linearLayoutManager);
        rvResults.setAdapter(listAdapter);
    }

    public void addResult(View view) {
    }
}
