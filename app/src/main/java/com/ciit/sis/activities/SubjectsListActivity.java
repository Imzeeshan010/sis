package com.ciit.sis.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ciit.sis.R;
import com.ciit.sis.adapters.SubjectListAdapter;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Subject;

import java.util.List;

public class SubjectsListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subjects_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Subjects");
        context= this;
        RecyclerView rvSubjects = findViewById(R.id.rv_subjects);
        SubjectListAdapter subjectListAdapter = new SubjectListAdapter(AppData.getInstance().getSubjects(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rvSubjects.setLayoutManager(linearLayoutManager);
        rvSubjects.setAdapter(subjectListAdapter);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

}
