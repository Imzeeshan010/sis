package com.ciit.sis.activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.adapters.TeacherQueriesListAdapter;
import com.ciit.sis.beans.Query;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.GetQueriesResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherQueriesActivity extends BaseActivity {

    private int subjectId;
    private RecyclerView rvQueries;
    private TeacherQueriesListAdapter listAdapter;
    private List<Query> queryList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_queries);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Queries");
        context = this;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            subjectId = bundle.getInt("subjectId", -1);
        }
        rvQueries = findViewById(R.id.rv_queries);

        showProgressDialog("Loading Queries...");

        ApiInterface apiService = ApiUtils.getAPIService();
        Call<GetQueriesResponse> call = apiService.getQueries(subjectId);
        call.enqueue(new Callback<GetQueriesResponse>() {
            @Override
            public void onResponse(Call<GetQueriesResponse> call, Response<GetQueriesResponse> response) {
                hideProgressDialog();
                if(!response.body().isSuccess()){
                    Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
                    return;
                }
                List<Query> serverQueryList = response.body().getData();
                queryList.clear();
                for(Query query: serverQueryList){
                    if(query.getIs_completed()==0) {
                        queryList.add(query);
                    }
                }
//                queryList = response.body().getData();
                updateList();
            }

            @Override
            public void onFailure(Call<GetQueriesResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "No queries found", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateList(){
        listAdapter = new TeacherQueriesListAdapter(queryList, context);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        rvQueries.setLayoutManager(linearLayoutManager);
        rvQueries.setAdapter(listAdapter);
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
