package com.ciit.sis.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.adapters.StudentsListAdapter;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Student;
import com.ciit.sis.beans.Subject;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.GetStudentsResponse;
import com.ciit.sis.net.models.PostResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadMarksActivity extends BaseActivity {
    private Button btnSubmit;
    private Spinner spnSubjects, spnStudents;
    private EditText etAss1, etAss2, etSess1, etSess2, etFinals;
    private List<Student> studentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_marks);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Upload Marks");
        context = this;
        btnSubmit = findViewById(R.id.btn_submit);
        spnSubjects = findViewById(R.id.spn_subjects);
        spnStudents = findViewById(R.id.spn_students);
        etAss1 = findViewById(R.id.et_ass1);
        etAss2 = findViewById(R.id.et_ass2);
        etSess1 = findViewById(R.id.et_sess1);
        etSess2 = findViewById(R.id.et_sess2);
        etFinals = findViewById(R.id.et_finals);
        ArrayAdapter<Subject> subjectArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, AppData.getInstance().getSubjects());
        spnSubjects.setAdapter(subjectArrayAdapter);
        spnSubjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Subject subject = AppData.getInstance().getSubjects().get(i);
                loadStudents(subject.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        if(AppData.getInstance().getSubjects().size()==0){
            Toast.makeText(context, "No subject for attendance.", Toast.LENGTH_SHORT).show();
        }
    }
    private void loadStudents(int subjectId){
        showProgressDialog("Loading Students...");

        ApiInterface apiService = ApiUtils.getAPIService();
        Call<GetStudentsResponse> call = apiService.getStudents(subjectId);
        call.enqueue(new Callback<GetStudentsResponse>() {
            @Override
            public void onResponse(Call<GetStudentsResponse> call, Response<GetStudentsResponse> response) {
                hideProgressDialog();
                if(response.body() ==null || !response.body().isSuccess()){
                    Toast.makeText(context, "No student found", Toast.LENGTH_SHORT).show();
                    return;
                }
                studentList = response.body().getData();
                updateList();
            }

            @Override
            public void onFailure(Call<GetStudentsResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "No student found", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void updateList(){
        if(studentList.size()>0){
            btnSubmit.setVisibility(View.VISIBLE);
        }
        ArrayAdapter listAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, studentList);
        spnStudents.setAdapter(listAdapter);
    }

    public void submitMarks(View view) {
        String ass1 = etAss1.getText().toString();
        String ass2 = etAss2.getText().toString();
        String sess2 = etSess1.getText().toString();
        String sess1 = etSess2.getText().toString();
        String finals = etFinals.getText().toString();
        if(ass1.equals("")){
            Toast.makeText(context, "Add Assignment 1 marks.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(ass2.equals("")){
            Toast.makeText(context, "Add Assignment 2 marks.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(sess1.equals("")){
            Toast.makeText(context, "Add Sessional 1 marks.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(sess2.equals("")){
            Toast.makeText(context, "Add Sessional 2 marks.", Toast.LENGTH_SHORT).show();
            return;
        }
        if(finals.equals("")){
            Toast.makeText(context, "Add Finals marks.", Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog("Saving result");
        Subject subjectSelected = (Subject) spnSubjects.getSelectedItem();
        Student studentSelected = (Student) spnStudents.getSelectedItem();

        HashMap<String, String> ass1Map = new HashMap<>();
        ass1Map.put(""+studentSelected.getId(), ass1);
        HashMap<String, String> ass2Map = new HashMap<>();
        ass2Map.put(""+studentSelected.getId(), ass2);
        HashMap<String, String> sess1Map = new HashMap<>();
        sess1Map.put(""+studentSelected.getId(), sess1);
        HashMap<String, String> sess2Map = new HashMap<>();
        sess2Map.put(""+studentSelected.getId(), sess2);
        HashMap<String, String> finalMap = new HashMap<>();
        finalMap.put(""+studentSelected.getId(), finals);

        MarksDTO marksDTO = new MarksDTO();
        marksDTO.setSubjectId(""+subjectSelected.getId());
        marksDTO.setAss1(ass1Map);
        marksDTO.setAss2(ass2Map);
        marksDTO.setMid1(sess1Map);
        marksDTO.setMid2(sess2Map);
        marksDTO.setFinals(finalMap);
        ApiInterface apiService = ApiUtils.getAPIService();
        Call<PostResponse> call = apiService.submitResult(marksDTO);
        call.enqueue(new Callback<PostResponse>() {
            @Override
            public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
                hideProgressDialog();
                if(response.body() ==null || !response.body().isSuccess()){
                    Toast.makeText(context, "Could not save result, try again later.", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(context,"Result submitted", Toast.LENGTH_SHORT).show();
                finish();
            }

            @Override
            public void onFailure(Call<PostResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(context, "Could not save result, try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
    public class MarksDTO{
        private String subjectId;
        private HashMap<String, String> ass1;
        private HashMap<String, String> ass2;
        private HashMap<String, String> mid1;
        private HashMap<String, String> mid2;
        @SerializedName("final")
        private HashMap<String, String> finals;

        public String getSubjectId() {
            return subjectId;
        }

        public void setSubjectId(String subjectId) {
            this.subjectId = subjectId;
        }

        public HashMap<String, String> getAss1() {
            return ass1;
        }

        public void setAss1(HashMap<String, String> ass1) {
            this.ass1 = ass1;
        }

        public HashMap<String, String> getAss2() {
            return ass2;
        }

        public void setAss2(HashMap<String, String> ass2) {
            this.ass2 = ass2;
        }

        public HashMap<String, String> getMid1() {
            return mid1;
        }

        public void setMid1(HashMap<String, String> mid1) {
            this.mid1 = mid1;
        }

        public HashMap<String, String> getMid2() {
            return mid2;
        }

        public void setMid2(HashMap<String, String> mid2) {
            this.mid2 = mid2;
        }

        public HashMap<String, String> getFinals() {
            return finals;
        }

        public void setFinals(HashMap<String, String> finals) {
            this.finals = finals;
        }
    }
}
