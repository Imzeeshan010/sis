package com.ciit.sis.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciit.sis.R;
import com.ciit.sis.beans.Attendance;

import java.util.List;

public class AttendanceListAdapter extends
    RecyclerView.Adapter<AttendanceListAdapter.AttendanceViewHolder> {

  private final List<Attendance> attendanceList;
  public AttendanceListAdapter(List<Attendance> attendanceList) {
    this.attendanceList = attendanceList;
  }

  @Override
  public void onBindViewHolder(AttendanceViewHolder holder, int position) {
    final Attendance tempAttendance= attendanceList.get(position);
    holder.tvSubjectName.setText(tempAttendance.getSubject_name());
    holder.tvTotalClass.setText("Total: "+tempAttendance.getTotal_classes());
    holder.tvAttendClass.setText("Present: "+tempAttendance.getAttend_classes());

  }

  @Override
  public AttendanceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.rv_attendance_item_list, parent, false);
    return new AttendanceViewHolder(view);
  }

  @Override
  public int getItemCount() {
    return attendanceList.size();
  }


  public class AttendanceViewHolder extends RecyclerView.ViewHolder {

    //public Button  ivPrint;
    private final View view;
    private final TextView tvSubjectName, tvTotalClass, tvAttendClass;

    AttendanceViewHolder(View itemView) {
      super(itemView);
      view = itemView;
      tvSubjectName = itemView.findViewById(R.id.tv_subject_name);
      tvTotalClass= itemView.findViewById(R.id.tv_total_class);
      tvAttendClass= itemView.findViewById(R.id.tv_attend_class);
    }
  }
}
