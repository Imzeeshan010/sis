package com.ciit.sis.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciit.sis.R;
import com.ciit.sis.beans.Query;

import java.util.List;

public class QueriesListAdapter extends
    RecyclerView.Adapter<QueriesListAdapter.QueryViewHolder> {

  private final List<Query> queryList;
  public QueriesListAdapter(List<Query> queryList) {
    this.queryList = queryList;
  }

  @Override
  public void onBindViewHolder(QueryViewHolder holder, int position) {
    final Query tempQuery= queryList.get(position);
    holder.tvQuestion.setText("Q: "+tempQuery.getQuestion());
    holder.tvAnswer.setText("Ans: "+tempQuery.getAnswer());
    holder.tvDate.setText(tempQuery.getCreated_at());

  }

  @Override
  public QueryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.rv_query_item_list, parent, false);
    return new QueryViewHolder(view);
  }

  @Override
  public int getItemCount() {
    return queryList.size();
  }


  public class QueryViewHolder extends RecyclerView.ViewHolder {

    //public Button  ivPrint;
    private final View view;
    private final TextView tvQuestion, tvAnswer, tvDate;

    QueryViewHolder(View itemView) {
      super(itemView);
      view = itemView;
      tvQuestion = itemView.findViewById(R.id.tv_question);
      tvAnswer= itemView.findViewById(R.id.tv_answer);
      tvDate= itemView.findViewById(R.id.tv_date);
    }
  }
}
