package com.ciit.sis.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciit.sis.R;
import com.ciit.sis.beans.Attendance;
import com.ciit.sis.beans.Result;

import java.util.List;

public class ResultsListAdapter extends
    RecyclerView.Adapter<ResultsListAdapter.ResultViewHolder> {

  private final List<Result> resultList;
  public ResultsListAdapter(List<Result> resultList) {
    this.resultList = resultList;
  }

  @Override
  public void onBindViewHolder(ResultViewHolder holder, int position) {
    final Result tempAttendance= resultList.get(position);
    holder.tvSubjectName.setText(tempAttendance.getSubject_name());
    holder.tvAssignment1.setText("Assignment 1: "+tempAttendance.getAss1());
    holder.tvAssignment2.setText("Assignment 2: "+tempAttendance.getAss2());
    holder.tvSessional1.setText("Sessional 1: "+tempAttendance.getMid1());
    holder.tvSessional2.setText("Sessional 2: "+tempAttendance.getMid2());
    holder.tvFinals.setText("Final : "+tempAttendance.getTotal());

  }

  @Override
  public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.rv_result_item_list, parent, false);
    return new ResultViewHolder(view);
  }

  @Override
  public int getItemCount() {
    return resultList.size();
  }


  public class ResultViewHolder extends RecyclerView.ViewHolder {

    //public Button  ivPrint;
    private final View view;
    private final TextView tvSubjectName, tvAssignment1, tvAssignment2, tvSessional1, tvSessional2, tvFinals;

    ResultViewHolder(View itemView) {
      super(itemView);
      view = itemView;
      tvSubjectName = itemView.findViewById(R.id.tv_subject_name);
      tvAssignment1= itemView.findViewById(R.id.tv_assign_1);
      tvAssignment2= itemView.findViewById(R.id.tv_assign_2);
      tvSessional1= itemView.findViewById(R.id.tv_sessional_1);
      tvSessional2= itemView.findViewById(R.id.tv_sessional_2);
      tvFinals= itemView.findViewById(R.id.tv_finals);
    }
  }
}
