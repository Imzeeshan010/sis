package com.ciit.sis.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.ciit.sis.R;
import com.ciit.sis.beans.Attendance;
import com.ciit.sis.beans.Student;
import com.ciit.sis.beans.User;

import java.util.List;

public class StudentsListAdapter extends
    RecyclerView.Adapter<StudentsListAdapter.StudentViewHolder> {

  private final List<Student> studentList;
  public StudentsListAdapter(List<Student> userList) {
    this.studentList = userList;
  }

  @Override
  public void onBindViewHolder(StudentViewHolder holder, int position) {
    final Student tempStudent= studentList.get(position);
    holder.tvName.setText("Name: "+tempStudent.getName());
    holder.tvId.setText("Id: "+tempStudent.getId());
    holder.cbAttendance.setOnCheckedChangeListener(null);

      //if true, your checkbox will be selected, else unselected
    holder.cbAttendance.setChecked(tempStudent.isPresent());
    holder.cbAttendance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            tempStudent.setPresent(b);
        }
    });
//    holder.view.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//            if(tempStudent.isPresent()) {
//                tempStudent.setPresent(false);
//            }
//            else{
//                tempStudent.setPresent(true);
//            }
//            holder.cbAttendance.setChecked(tempStudent.isPresent());
//        }
//    });
  }

  @Override
  public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.rv_student_item_list, parent, false);
    return new StudentViewHolder(view);
  }

  @Override
  public int getItemCount() {
    return studentList.size();
  }


  public class StudentViewHolder extends RecyclerView.ViewHolder {

    //public Button  ivPrint;
    private final View view;
    private final TextView tvName, tvId;
    private final CheckBox cbAttendance;

    StudentViewHolder(View itemView) {
      super(itemView);
//        this.setIsRecyclable(false);
      view = itemView;
      tvName = itemView.findViewById(R.id.tv_name);
      tvId= itemView.findViewById(R.id.tv_id);
      cbAttendance= itemView.findViewById(R.id.cb_attendance);
    }
  }
}
