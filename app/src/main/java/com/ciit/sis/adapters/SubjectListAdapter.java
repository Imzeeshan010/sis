package com.ciit.sis.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ciit.sis.R;
import com.ciit.sis.activities.QueriesListActivity;
import com.ciit.sis.activities.TeacherQueriesActivity;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Subject;

import java.util.List;

public class SubjectListAdapter extends
    RecyclerView.Adapter<SubjectListAdapter.SubjectViewHolder> {

  private final List<Subject> subjectList;
  private Context context;
  public SubjectListAdapter(List<Subject> subjectList, Context context) {
    this.subjectList = subjectList;
    this.context = context;
  }

  @Override
  public void onBindViewHolder(SubjectViewHolder holder, int position) {
    final Subject tempSubject = subjectList.get(position);
    holder.tvSubject.setText(tempSubject.getName());
    holder.view.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent ;
        if(AppData.getInstance().getUser().getUserable_type().equals("Student")){
          intent = new Intent(context, QueriesListActivity.class);
        }
        else{
          intent = new Intent(context, TeacherQueriesActivity.class);
        }
        Bundle bundle = new Bundle();
        bundle.putInt("subjectId", tempSubject.getId());
        intent.putExtras(bundle);
        context.startActivity(intent);
      }
    });
  }

  @Override
  public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view =  LayoutInflater.from(parent.getContext())
        .inflate(R.layout.rv_subject_item_list, parent, false);

    return new SubjectViewHolder(view);
  }

  @Override
  public int getItemCount() {
    return subjectList.size();
  }


  public class SubjectViewHolder extends RecyclerView.ViewHolder {

    private final View view;
    private final TextView tvSubject;

    SubjectViewHolder(View itemView) {
      super(itemView);
      view = itemView;
      tvSubject = itemView.findViewById(R.id.tv_subject_name);
    }
  }
}
