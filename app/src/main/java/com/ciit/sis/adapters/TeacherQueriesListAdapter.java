package com.ciit.sis.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ciit.sis.R;
import com.ciit.sis.activities.QueriesListActivity;
import com.ciit.sis.app.AppData;
import com.ciit.sis.beans.Query;
import com.ciit.sis.net.ApiInterface;
import com.ciit.sis.net.ApiUtils;
import com.ciit.sis.net.models.PostResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeacherQueriesListAdapter extends
    RecyclerView.Adapter<TeacherQueriesListAdapter.QueryViewHolder> {

  ProgressDialog progressDialog;
  private final List<Query> queryList;
  private Context context;
  public TeacherQueriesListAdapter(List<Query> queryList, Context context) {
    this.queryList = queryList;
    this.context = context;
  }

  @Override
  public void onBindViewHolder(final QueryViewHolder holder, final int position) {
    final Query tempQuery= queryList.get(position);
    holder.tvQuestion.setText("Q: "+tempQuery.getQuestion());
    final EditText etAnswer = holder.etAnswer;
    holder.btnSubmit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(etAnswer.getText().toString().equals("")){
                Toast.makeText(context, "Enter anwser", Toast.LENGTH_SHORT).show();
                return;
            }
            tempQuery.setAnswer(etAnswer.getText().toString());
            postQueryToServer(tempQuery, holder.getAdapterPosition());
        }
    });
  }

  @Override
  public QueryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.rv_teacher_query_item_list, parent, false);
    return new QueryViewHolder(view);
  }

  @Override
  public int getItemCount() {
    return queryList.size();
  }
  private void postQueryToServer(Query query,final int position){
    showProgressDialog("Saving Queries...");
    final int studentId = AppData.getInstance().getUser().getUserable_id();
  Map<String, String> params = new HashMap<>();
      params.put("id",""+query.getId());
  params.put("studentId",""+studentId);
  params.put("subjectId",""+query.getSubject_id());
  params.put("query",query.getQuestion());
  params.put("answer",query.getAnswer());
    ApiInterface apiService = ApiUtils.getAPIService();
    Call<PostResponse> call = apiService.postQuery(params);
    call.enqueue(new Callback<PostResponse>() {
      @Override
      public void onResponse(Call<PostResponse> call, Response<PostResponse> response) {
        hideProgressDialog();
        if(response.body()==null || !response.body().isSuccess()){
          Toast.makeText(context, "Problem posting query. Try again later.", Toast.LENGTH_SHORT).show();
          return;
        }
        queryList.remove(position);
        notifyDataSetChanged();
      }

      @Override
      public void onFailure(Call<PostResponse> call, Throwable t) {
        hideProgressDialog();
        Toast.makeText(context, "Problem posting query. Try again later.", Toast.LENGTH_SHORT).show();
      }
    });
  }
    void showProgressDialog(String msg) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setIndeterminate(true);
        }
        progressDialog.setMessage(msg);
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    void hideProgressDialog() {
        Handler mainHandler = new Handler(context.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.hide();
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        });
    }
  public class QueryViewHolder extends RecyclerView.ViewHolder {

    //public Button  ivPrint;
    private final View view;
    private final TextView tvQuestion;
    private final EditText etAnswer;
    private final Button btnSubmit;

    QueryViewHolder(View itemView) {
      super(itemView);
      view = itemView;
      tvQuestion = itemView.findViewById(R.id.tv_question);
      etAnswer= itemView.findViewById(R.id.et_answer);
      btnSubmit= itemView.findViewById(R.id.btn_submit);
    }
  }
}
