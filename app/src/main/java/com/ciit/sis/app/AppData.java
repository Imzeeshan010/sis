package com.ciit.sis.app;

import com.ciit.sis.beans.Subject;
import com.ciit.sis.beans.User;

import java.util.List;

public class AppData {
    private static final AppData instance = new AppData();

    //private constructor to avoid client applications to use constructor
    private AppData(){}
    private User user;
    private List<Subject> subjects;
    public static AppData getInstance(){
        return instance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
