package com.ciit.sis.beans;

public class Attendance {
    private int id;
    private String subject_name;
    private int total_classes;
    private int attend_classes;

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getSubject_name() {
        return this.subject_name;
    }
    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }
    public int getTotal_classes() {
        return this.total_classes;
    }
    public void setTotal_classes(int total_classes) {
        this.total_classes = total_classes;
    }
    public int getAttend_classes() {
        return this.attend_classes;
    }
    public void setAttend_classes(int attend_classes) {
        this.attend_classes = attend_classes;
    }
}
