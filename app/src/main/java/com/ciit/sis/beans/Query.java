package com.ciit.sis.beans;

public class Query {
    private int id;
    private int subject_id;
    private int student_id;
    private String question;
    private String answer;
    private int is_completed;
    private String created_at;
    private String updated_at;

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getSubject_id() {
        return this.subject_id;
    }
    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }
    public int getStudent_id() {
        return this.student_id;
    }
    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }
    public String getQuestion() {
        return this.question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }
    public String getAnswer() {
        return this.answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }
    public int getIs_completed() {
        return this.is_completed;
    }
    public void setIs_completed(int is_completed) {
        this.is_completed = is_completed;
    }
    public String getCreated_at() {
        return this.created_at;
    }
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    public String getUpdated_at() {
        return this.updated_at;
    }
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
