package com.ciit.sis.beans;

import com.google.gson.annotations.SerializedName;

public class Result {
    private int id;
    private String subject_name;
    private int ass1;
    private int ass2;
    private int mid1;
    private int mid2;
    @SerializedName("final")
    private int total;

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getSubject_name() {
        return this.subject_name;
    }
    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }
    public int getAss1() {
        return this.ass1;
    }
    public void setAss1(int ass1) {
        this.ass1 = ass1;
    }
    public int getAss2() {
        return this.ass2;
    }
    public void setAss2(int ass2) {
        this.ass2 = ass2;
    }
    public int getMid1() {
        return this.mid1;
    }
    public void setMid1(int mid1) {
        this.mid1 = mid1;
    }
    public int getMid2() {
        return this.mid2;
    }
    public void setMid2(int mid2) {
        this.mid2 = mid2;
    }
    public int getTotal() {
        return this.total;
    }
    public void setTotal(int total) {
        this.total = total;
    }
}
