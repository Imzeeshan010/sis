package com.ciit.sis.beans;

public class Subject {
    private int id;
    private String name;
    private int score;
    private String created_at;
    private String updated_at;
    private int level_id;
    private int teacher_id;

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getScore() {
        return this.score;
    }
    public void setScore(int score) {
        this.score = score;
    }
    public String getCreated_at() {
        return this.created_at;
    }
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    public String getUpdated_at() {
        return this.updated_at;
    }
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
    public int getLevel_id() {
        return this.level_id;
    }
    public void setLevel_id(int level_id) {
        this.level_id = level_id;
    }
    public int getTeacher_id() {
        return this.teacher_id;
    }
    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    @Override
    public String toString() {
        return name;
    }
}
