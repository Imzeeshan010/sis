package com.ciit.sis.beans;

public class User {
    private int id;
    private String name;
    private String email;
    private String created_at;
    private String updated_at;
    private String student_id;
    private int userable_id;
    private String userable_type;

    public int getId() {
        return this.id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return this.email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getCreated_at() {
        return this.created_at;
    }
    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    public String getUpdated_at() {
        return this.updated_at;
    }
    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public int getUserable_id() {
        return this.userable_id;
    }
    public void setUserable_id(int userable_id) {
        this.userable_id = userable_id;
    }
    public String getUserable_type() {
        return this.userable_type;
    }
    public void setUserable_type(String userable_type) {
        this.userable_type = userable_type;
    }
}
