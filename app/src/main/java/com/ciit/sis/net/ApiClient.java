package com.ciit.sis.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by adil on 8/15/17.
 */

class ApiClient {

  //    public static final String BASE_URL = "https://jsonplaceholder.typicode.com/";
  private static Retrofit retrofit = null;


  public static Retrofit getClient(String url) {
    if (retrofit == null) {
      final OkHttpClient okHttpClient = getUnsafeOkHttpClient()
          .readTimeout(60, TimeUnit.SECONDS)
          .connectTimeout(60, TimeUnit.SECONDS)
          .build();

      Gson gson;//= new Gson();
      GsonBuilder gsonBuilder = new GsonBuilder();
      gsonBuilder.setDateFormat("MM/dd/yyyy hh:mm:ss a");
      gson = gsonBuilder.create();
      retrofit = new Retrofit.Builder()
          .baseUrl(url)
//          .callbackExecutor(Executors.newSingleThreadExecutor())
          .addConverterFactory(GsonConverterFactory.create(gson))
          .client(okHttpClient)
          .build();
    }
    return retrofit;
  }
  public static OkHttpClient.Builder getUnsafeOkHttpClient() {
    try {
      // Create a trust manager that does not validate certificate chains
      final TrustManager[] trustAllCerts = new TrustManager[]{
              new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                  return new java.security.cert.X509Certificate[]{};
                }
              }
      };

      // Install the all-trusting trust manager
      final SSLContext sslContext = SSLContext.getInstance("SSL");
      sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

      // Create an ssl socket factory with our all-trusting manager
      final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

      OkHttpClient.Builder builder = new OkHttpClient.Builder();
      builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
      builder.hostnameVerifier(new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      });
      return builder;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
