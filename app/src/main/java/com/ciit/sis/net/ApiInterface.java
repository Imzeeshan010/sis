package com.ciit.sis.net;

import com.ciit.sis.activities.UploadMarksActivity;
import com.ciit.sis.net.models.GetAttendanceResponse;
import com.ciit.sis.net.models.GetQueriesResponse;
import com.ciit.sis.net.models.GetResultResponse;
import com.ciit.sis.net.models.GetStudentsResponse;
import com.ciit.sis.net.models.LoginResponse;
import com.ciit.sis.net.models.PostResponse;

import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
public interface ApiInterface {
  @GET("login")
  Call<LoginResponse> login(@Header("email") String userEmail, @Header("password") String password);

  @GET("getStudentAttendence")
  Call<GetAttendanceResponse> getAttendance(@Query("studentId") int studentId);

  @GET("getStudentResult")
  Call<GetResultResponse> getResult(@Query("studentId") int studentId);


  @GET("getStudentsRelatedSubject")
  Call<GetStudentsResponse> getStudents(@Query("subjectId") int subjectId);

  @GET("getSubjectQueries")
  Call<GetQueriesResponse> getQueries(@Query("subjectId") int subjectId);

//  @FormUrlEncoded
  @POST("postQuery")
//  @Headers("Content-Type:application/x-www-form-urlencoded")
  Call<PostResponse> postQuery(@Body Map<String, String> params);

//  @FormUrlEncoded
  @POST("addAttendence")
  Call<PostResponse> submitAttendance(@Body Map<String, String> params);

//  @FormUrlEncoded
  @POST("postStudentResult")
  Call<PostResponse> submitResult(@Body UploadMarksActivity.MarksDTO params);

}
