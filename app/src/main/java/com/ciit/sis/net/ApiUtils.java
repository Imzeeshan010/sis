package com.ciit.sis.net;


import com.ciit.sis.constants.Constants;

public class ApiUtils {

  private ApiUtils() {
  }

  public static ApiInterface getAPIService() {
    return ApiClient.getClient(Constants.BASE_URL).create(ApiInterface.class);
  }
}
