package com.ciit.sis.net.models;

import com.ciit.sis.beans.Attendance;
import java.util.List;

/**
 * Created by adil on 3/3/18.
 */

public class GetAttendanceResponse {
    boolean success;
    String message;
    List<Attendance> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Attendance> getData() {
        return data;
    }

    public void setData(List<Attendance> data) {
        this.data = data;
    }
}
