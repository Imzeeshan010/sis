package com.ciit.sis.net.models;

import com.ciit.sis.beans.Query;
import com.ciit.sis.beans.Result;

import java.util.List;

/**
 * Created by adil on 3/3/18.
 */

public class GetQueriesResponse {
    boolean success;
    String message;
    List<Query> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Query> getData() {
        return data;
    }

    public void setData(List<Query> data) {
        this.data = data;
    }
}
