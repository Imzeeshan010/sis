package com.ciit.sis.net.models;

import com.ciit.sis.beans.Attendance;
import com.ciit.sis.beans.Result;

import java.util.List;
import java.util.Map;

/**
 * Created by adil on 3/3/18.
 */

public class GetResultResponse {
    boolean success;
    String message;
    Map<String, Result> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Result> getData() {
        return data;
    }

    public void setData(Map<String, Result> data) {
        this.data = data;
    }
}
