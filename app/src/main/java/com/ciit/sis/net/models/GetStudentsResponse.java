package com.ciit.sis.net.models;

import com.ciit.sis.beans.Attendance;
import com.ciit.sis.beans.Student;

import java.util.List;

/**
 * Created by adil on 3/3/18.
 */

public class GetStudentsResponse {
    boolean success;
    String message;
    List<Student> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Student> getData() {
        return data;
    }

    public void setData(List<Student> data) {
        this.data = data;
    }
}
