package com.ciit.sis.net.models;

import com.ciit.sis.beans.Attendance;
import com.ciit.sis.beans.Subject;
import com.ciit.sis.beans.User;

import java.util.List;

/**
 * Created by adil on 3/3/18.
 */

public class LoginResponse {
    boolean success;
    String message;
    DataModel data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataModel getData() {
        return data;
    }

    public void setData(DataModel data) {
        this.data = data;
    }

    public class DataModel {
        User user;
        List<Subject> subjects;

        public User getUser() {
            return user;
        }

        public void setUser(User user) {
            this.user = user;
        }

        public List<Subject> getSubjects() {
            return subjects;
        }

        public void setSubjects(List<Subject> subjects) {
            this.subjects = subjects;
        }
    }
}
